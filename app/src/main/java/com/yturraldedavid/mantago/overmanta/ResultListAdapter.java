package com.yturraldedavid.mantago.overmanta;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import com.yturraldedavid.mantago.R;
import com.yturraldedavid.mantago.datalevel.DataObject;

public class ResultListAdapter extends BaseAdapter {
    private Context mContext;
    Cursor cursor;
    public Activity mActivity;
    public ArrayList<DataObject> m_array;

    public ResultListAdapter(Context context,Cursor cur)
    {
        super();
        mContext=context;
        cursor=cur;
    }

    @Override
    public int getCount() {
        return  m_array.size();
    }

    @Override
    public Object getItem(int position) {return position;}

    @Override
    public long getItemId(int position) {return position;}

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        RelativeLayout vp = (RelativeLayout)inflater.inflate(R.layout.activity_result_row, null);
        DataObject object = m_array.get(position);

        ImageView thumbnail = (ImageView) vp.findViewById(R.id.thumbnail);
        thumbnail.setImageBitmap((Bitmap) object.get("image") );
        TextView tittle = (TextView) vp.findViewById(R.id.textViewNombreLugar) ;
        tittle.setText((String) object.get("name"));



        convertView = vp;
        return convertView;
    }
}
