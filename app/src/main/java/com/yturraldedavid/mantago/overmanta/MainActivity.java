package com.yturraldedavid.mantago.overmanta;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.yturraldedavid.mantago.R;
import com.yturraldedavid.mantago.datalevel.DataQuery;
import com.yturraldedavid.mantago.datalevel.FindCallback;
import com.yturraldedavid.mantago.datalevel.DataException;
import com.yturraldedavid.mantago.datalevel.DataLowLevel;
import com.yturraldedavid.mantago.datalevel.DataObject;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    String opcion = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.lugares_turisticos) {
            Intent intent = new Intent (MainActivity.this, ResultActivity.class);
            startActivity(intent);
        } else if (id == R.id.playas) {
            opcion = "playa";
            Intent intent = new Intent (MainActivity.this, ResultActivity.class);
            intent.putExtra("opcion",opcion);
            startActivity(intent);
        } else if (id == R.id.restaurantes) {
            opcion = "restaurant";
            Intent intent = new Intent (MainActivity.this, ResultActivity.class);
            intent.putExtra("opcion",opcion);
            startActivity(intent);
        } else if (id == R.id.bares) {
            opcion = "bar";
            Intent intent = new Intent (MainActivity.this, ResultActivity.class);
            intent.putExtra("opcion",opcion);
            startActivity(intent);
        } else if (id == R.id.hoteles) {
            opcion = "hotel";
            Intent intent = new Intent(MainActivity.this, ResultActivity.class);
            intent.putExtra("opcion",opcion);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
