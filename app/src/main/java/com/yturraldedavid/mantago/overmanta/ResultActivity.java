package com.yturraldedavid.mantago.overmanta;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import com.yturraldedavid.mantago.R;

import com.yturraldedavid.mantago.datalevel.DataException;
import com.yturraldedavid.mantago.datalevel.DataObject;
import com.yturraldedavid.mantago.datalevel.DataQuery;
import com.yturraldedavid.mantago.datalevel.FindCallback;

import java.util.ArrayList;

public class ResultActivity extends AppCompatActivity implements ListView.OnItemClickListener{

    private ListView listViewLugares;
    public ResultListAdapter m_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        listViewLugares = (ListView) findViewById(R.id.listViewLugares);

        String opcion = getIntent().getExtras().getString("opcion");
        DataQuery query = DataQuery.get("playa");
        query.findInBackground("", "", DataQuery.OPERATOR_ALL, new FindCallback<DataObject>() {
            @Override
            public void done(ArrayList<DataObject> dataObjects, DataException e) {
                if (e == null) {
                    if (dataObjects.size() != 0) {
                        m_adapter = new ResultListAdapter(ResultActivity.this, null);

                        m_adapter.m_array = dataObjects;
                        m_adapter.mActivity = ResultActivity.this;

                        listViewLugares.setAdapter(m_adapter);
                    }
                } else {
                    // Error

                }
            }
        });


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        DataObject object = (DataObject) m_adapter.m_array.get(position);
        Intent intent;
        intent = new Intent(this, DetailActivity.class);
        intent.putExtra("object_id", object.m_objectId);
        startActivity(intent);
    }

}
