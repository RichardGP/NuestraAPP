package com.yturraldedavid.mantago.datalevel;

import java.util.ArrayList;

public interface FindCallback<DataObject> {
    public void done(ArrayList<DataObject> objects, DataException e);
}
