package com.yturraldedavid.mantago.datalevel;

public interface GetCallback <DataObject>{
    public void done(DataObject object, DataException e);
}
