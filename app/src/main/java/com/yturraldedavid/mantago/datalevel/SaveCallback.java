package com.yturraldedavid.mantago.datalevel;

public interface SaveCallback<DataObject> {
    public void done(DataObject object, DataException e);
}
